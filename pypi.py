import subprocess
import tempfile
from pathlib import Path
import pkginfo
import re
from packaging import requirements, version
import json


root = Path(__file__).parent / 'prefix'
pip = 'pip'


class Package:
    name = None
    version = None
    dependencies = []

    def _dict(self):
        return {
            "name": self.name,
            "version": str(self.version),
            "prefix": self.prefix.absolute().as_posix(),
            "dependencies": [
                {"name": i.name, "version": str(i.version)}
                for i in self.dependencies
            ]
        }

    @staticmethod
    def to_json(pkgs):
        return json.dumps(pkgs, default=lambda o: o._dict())

    @classmethod
    def _from_dict(cls, dic):
        self = cls()
        self.name = dic['name']
        self.version = version.parse(dic['version'])
        self.prefix = Path(dic['prefix'])
        self._dependencies = [
            (i['name'], version.parse(i['version']))
            for i in dic['dependencies']
        ]

        return ((self.name, self.version), self)

    @classmethod
    def from_json(cls, js):
        all_packages = dict(cls._from_dict(i) for i in json.loads(js))
        for pkg in all_packages.values():
            pkg.dependencies = [all_packages[i] for i in pkg._dependencies]

        return list(all_packages.values())

    @classmethod
    def from_wheel(cls, wheel):
        self = cls()
        self.name = wheel.name
        self.version = version.parse(wheel.version)
        self._wheel = wheel
        self._requirements = [
            requirements.Requirement(i)
            for i in wheel.requires_dist
        ]

        self.prefix = root/f"{self.name}=={self.version}"

        return (
            (wheel.name, self.version),
            self
        )

    def _link_deps(self, all_packages):
        self.dependencies = []
        for req in self._requirements:
            for (n, v), pkg in all_packages.items():
                if n != req.name:
                    continue
                if not req.specifier.contains(v):
                    continue
                self.dependencies.append(pkg)
        return self

    def __repr__(self):
        return f"<Package('{self.name}=={self.version}')>"


def install_wheel(wheel, prefix):
    code = subprocess.Popen([
        'pip', 'install',
        '--no-deps', '--no-index', '--target', prefix, wheel.filename
    ]).wait()
    assert code == 0


def resolve(pkgs, install=False):
    with tempfile.TemporaryDirectory() as tmpdir:
        my_tmpdir = Path(tmpdir)
        code = subprocess.Popen(
            [pip, 'download', '--log', my_tmpdir/'log.txt'] + pkgs,
            cwd=my_tmpdir
        ).wait()
        assert code == 0

        log = (my_tmpdir/'log.txt').read_text()

        wheels = [
            pkginfo.get_metadata((my_tmpdir/pkg).as_posix())
            for pkg in re.findall(r"Saved \./(.*whl)", log)
        ]

        all_packages = dict(
            Package.from_wheel(wheel)
            for wheel in wheels
        )

        if install:
            for i in all_packages.values():
                install_wheel(i._wheel, i.prefix)

        return [
            i._link_deps(all_packages)
            for i in all_packages.values()
        ]


if __name__ == '__main__':
    pkgs = resolve(['matplotlib'], False)
    (root/'database.json').write_text(Package.to_json(pkgs))
